import Issue from './pages/Issue'

describe('Conclusions', () => {
  it('creates a Good Practice', () => {
    const description = 'good practice text'

    const issue = new Issue()
      .createIssue()
      .openNewGoodPractice()
      .closeConclusionModal()
      .openNewGoodPractice()
      .fillConclusion(description)
      .addConclusion()

    expect(issue.includes('You have declared a new good practice!')).to.be.true
  })

  it('creates a Lesson Learned', () => {
    const description = 'lesson learned text'

    const issue = new Issue()
      .createIssue()
      .openNewLessonLearned()
      .closeConclusionModal()
      .openNewLessonLearned()
      .fillConclusion(description)
      .addConclusion()

    expect(issue.includes('You have declared a new lesson learned!')).to.be.true
  })
})
