import Actions from '../api/actions'
import Service from './Service'

export default class GoodPractices extends Service {
  subscriptions() {
    this.bus.subscribe('create.goodPractice', this.create.bind(this))
    this.bus.subscribe('retrieve.goodPractices', this.retrieveAllFor.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.goodPractice')

    const goodPractice = Actions.createGoodPractice.do(payload)

    callback(goodPractice)
  }

  retrieveAllFor({ issue }){
    let callback = this.buildCallback('retrieved.goodPractices')

    const goodPractice = Actions.retrieveGoodPractices.do(issue)

    callback(goodPractice)
  }
}
