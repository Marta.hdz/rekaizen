import RetrieveAnalyses from '../api/actions/RetrieveAnalyses'
import RemoveAnalysis from '../api/actions/RemoveAnalysis'
import Actions from '../api/actions'
import Service from './Service'

export default class Positives extends Service {
  subscriptions() {
    this.bus.subscribe('create.positive', this.create.bind(this))
    this.bus.subscribe('remove.positive', this.removePositive.bind(this))
    this.bus.subscribe('retrieve.positives', this.retrieveAllFor.bind(this))
  }

  create(payload) {
    let callback = this.buildCallback('created.positive')

    const positive = Actions.createPositive.do(payload)

    callback(positive)
  }

  retrieveAllFor(payload){
    const callback = this.buildCallbackRetrieve.bind(this)

    const analyses = RetrieveAnalyses.do(payload)

    callback(analyses)
  }

  removePositive(payload) {
    let callback = this.buildCallback('removed.positive')

    const issue = RemoveAnalysis.do(payload)

    callback(issue.analyses)
  }

  buildCallbackRetrieve(response) {
    this.bus.publish('retrieved.positives', response.positives)
  }
}
