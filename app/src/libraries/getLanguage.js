const SUPPORTED_LANGUAGES = ['es', 'en']

function isSupported(language) {
  return SUPPORTED_LANGUAGES.includes(language)
}

export default function() {
  const navigatorLanguage = window.navigator.language.slice(0, 2).toLowerCase()
  let language = 'es'
  if (isSupported(navigatorLanguage)) {
    language = navigatorLanguage
  }
  return language
}
