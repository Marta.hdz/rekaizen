function classNames (defaultClass = '', classesNames = {}, customClasses = '') {
  const classes = Object.keys(classesNames).filter(key => classesNames[key])
  return [defaultClass, ...classes, customClasses].join(' ').trim()
}

export default classNames
