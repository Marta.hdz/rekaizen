import SecureRandom from '../libraries/SecureRandom'
import Dates from '../libraries/Dates'

const NEGATIVE = 'negative'
const POSITIVE = 'positive'

export default class Analysis {
  static asNull() {
    const analysis = new Analysis(null, null, null)

    analysis.birthday = null
    analysis.id = null

    return analysis
  }

  static from(document) {
      const analysis = new Analysis(document.description, document.type)

      analysis.birthday = document.birthday
      analysis.id = document.id

      return analysis
  }
  static negativeFrom(document) {
    const negative = this.from(document)

    negative.type = NEGATIVE

    return negative
  }

  static positiveFrom(document) {
    const positive = this.from(document)

    positive.type = POSITIVE

    return positive
  }

  static asNegative(description) {
    const negative = new Analysis(description, NEGATIVE)

    return negative
  }

  static asPositive(description) {
    const positive = new Analysis(description, POSITIVE)

    return positive
  }

  constructor(description, type) {
    this.birthday = this.generateTimestamp()
    this.description = description
    this.type = type
    this.id = this.generateId()
  }

  generateId() {
    return SecureRandom.uuid()
  }

  generateTimestamp(){
    return Dates.generateTimestamp()
  }

  serialize() {
    return {
      'description': this.description,
      'birthday': this.birthday,
      'type': this.type,
      'id': this.id
    }
  }

  isEqualTo(analysis) {
    return this.id == analysis.id && this.type == analysis.type
  }

  is(id) {
    return this.id == id
  }

  isNegative() {
    return (this.type == NEGATIVE)
  }

  isPositive() {
    return (this.type == POSITIVE)
  }
}
