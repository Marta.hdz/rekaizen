import SecureRandom from '../libraries/SecureRandom'
import Dates from '../libraries/Dates'

const LESSON_LEARNED_TYPE = 'lessonLearned'
const GOOD_PRACTICE_TYPE = 'goodPractice'

export default class Conclusion {
  static asNull() {
    const conclusion = new Conclusion(null, null)

    conclusion.birthday = null
    conclusion.id = null

    return conclusion
  }

  static asGoodPractice(description) {
    return new Conclusion(description, GOOD_PRACTICE_TYPE)
  }

  static asLessonLearned(description) {
    return new Conclusion(description, LESSON_LEARNED_TYPE)
  }

  constructor(description, type){
    this.id = this.generateId()
    this.birthday = this.generateTimestamp()
    this.description = description
    this.type = type
  }

  generateId() {
    return SecureRandom.uuid()
  }

  generateTimestamp(){
    return Dates.generateTimestamp()
  }

  serialize() {
    return {
      'id': this.id,
      'description': this.description,
      'birthday': this.birthday,
      'type': this.type
    }
  }

  isAGoodPractice() {
    return (this.type == GOOD_PRACTICE_TYPE)
  }

  static from(document) {
    const conclusion = new Conclusion(document.description)
    conclusion.id = document.id
    conclusion.birthday = document.birthday
    conclusion.type = document.type

    return conclusion
  }

  is(id) {
    return this.id == id
  }

  isALessonLearned() {
    return (this.type == LESSON_LEARNED_TYPE)
  }
}
