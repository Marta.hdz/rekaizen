import ImprovementActions from '../services/improvementActions'
import Translations from '../services/translations'
import Conclusions from '../services/conclusions'
import Reports from '../services/reports'
import Issues from '../services/issues'

export default class GenerateReport {
  static do(id, language) {
    Issues.service.finalize(id)
    const issue = Issues.service.retrieve(id)
    const positives = Issues.service.retrievePositives(id)
    const negatives = Issues.service.retrieveNegatives(id)
    const goodPractices = Conclusions.service.retrieveAllGoodPractices(
      issue.conclusions,
    )
    const lessonsLearned = Conclusions.service.retrieveAllLessonsLearned(
      issue.conclusions,
    )
    const improvementActions = ImprovementActions.service.retrieve(
      issue.improvementActions,
    )
    const translations = Translations.collection.retrieveReportTranslations(
      language,
    )
    const report = Reports.service.generate(
      issue,
      positives,
      negatives,
      goodPractices,
      lessonsLearned,
      improvementActions,
      translations,
    )

    return report
  }
}
