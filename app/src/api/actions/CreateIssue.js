import Issues from '../services/issues'

export default class CreateIssue {
  static do(payload) {
    return Issues.service.create(payload.description, payload.timebox)
  }
}
