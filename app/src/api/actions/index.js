import CreateGoodPractice from './CreateGoodPractice'
import CreateLessonLearned from './CreateLessonLearned'
import CreateNegative from './CreateNegative'
import CreatePositive from './CreatePositive'
import CreateImprovementAction from './CreateImprovementAction'
import GenerateReport from './GenerateReport'
import RetrieveAnalyses from './RetrieveAnalyses'
import RemoveAnalysis from './RemoveAnalysis'
import RemoveImprovementAction from './RemoveImprovementAction'
import RetrieveImprovementActions from './RetrieveImprovementActions'
import RetrieveGoodPractices from './RetrieveGoodPractices'
import RetrieveLessonsLearned from './RetrieveLessonsLearned'
import RetrieveTimeBoxes from './RetrieveTimeBoxes'
import CreateIssue from './CreateIssue'
import TranslateAll from './TranslateAll'

const Actions = {
  translateAll: (bus) => new TranslateAll(bus),
  retrieveTimeBoxes: (bus) => new RetrieveTimeBoxes(bus),
  retrieveImprovementActions: RetrieveImprovementActions,
  retrieveLessonsLearned: (bus) => new RetrieveLessonsLearned(bus),
  retrieveGoodPractices: RetrieveGoodPractices,
  retrieveAnalyses: (bus) => new RetrieveAnalyses(bus),
  removeAnalysis: (bus) => new RemoveAnalysis(bus),
  removeImprovementAction: RemoveImprovementAction,
  generateReport: GenerateReport,
  createPositive: CreatePositive,
  createNegative: (bus) => new CreateNegative(bus),
  createLessonLearned: (bus) => new CreateLessonLearned(bus),
  createGoodPractice: CreateGoodPractice,
  createImprovementAction: CreateImprovementAction,
  createIssue: CreateIssue
}

export default Actions
