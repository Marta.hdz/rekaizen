import Conclusions from '../services/conclusions'
import Issues from '../services/issues'

export default class CreateGoodPractice {
  static do(goodPractice) {
    const newGoodPractice = Conclusions.service.createGoodPractice(goodPractice.description)

    Issues.service.addConclusion(goodPractice.issue, newGoodPractice.id)

    return newGoodPractice
  }
}
