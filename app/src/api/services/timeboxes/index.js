import Collection from './collection'
import Service from './service'

const Timeboxes = {
  collection: Collection,
  service: Service
}

export default Timeboxes
