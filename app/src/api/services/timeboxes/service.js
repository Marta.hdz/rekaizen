import Collection from './collection'

export default class Service {
  static retrieveAll () {
    return Collection.retrieveAll()
  }
}
