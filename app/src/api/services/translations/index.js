import Collection from './collection'
import Service from './service'

const Translation = {
  collection: Collection,
  service: Service
}

export default Translation
