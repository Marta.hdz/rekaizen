import Report from '../../domain/Report'

export default class Service {
  static generate(issue, positives, negatives, goodPractices, lessonsLearned, improvementActions, translations) {
    const report = new Report(issue, translations)

    report.addDataForIntroduction(positives.length, negatives.length)
    report.addDataForGoodPractices(goodPractices)
    report.addDataForLessonsLearned(lessonsLearned)
    report.addDataForImprovementActions(improvementActions)

    return report.serialize()
  }
}
