import Collection from './collection'
import Service from './service'

const ImprovementActions = {
  service: Service,
  collection: Collection
}

export default ImprovementActions
