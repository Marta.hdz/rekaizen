import ImprovementAction from '../../domain/ImprovementAction'
import Collection from './collection'

export default class Service {
  static create(description) {
    const improvementAction = new ImprovementAction(description)

    const stored = Collection.insert(improvementAction)

    return stored.serialize()
  }

  static retrieve(references) {
    const retrieved = Collection.retrieve(references)

    return retrieved.map((improvementAction) => {
      return improvementAction.serialize()
    })
  }

  static remove(id) {
    return Collection.remove(id)
  }
}
