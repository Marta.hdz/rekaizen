import Conclusion from '../../domain/Conclusion'
import Collection from './collection'

export default class Service {
  static createGoodPractice(description) {
    let goodPractice = Conclusion.asGoodPractice(description)
    return Collection.createGoodPractice(goodPractice)
  }

  static retrieveAllGoodPractices(conclusions) {
    let allGoodPractices = Collection.retrieveAllGoodPractices(conclusions)

    let serialized = allGoodPractices.map((aGoodPractice) => {
      return aGoodPractice.serialize()
    })

    return serialized
  }

  static createLessonLearned(description) {
    let lessonLearned = Conclusion.asLessonLearned(description)
    return Collection.createLessonLearned(lessonLearned)
  }

  static retrieveAllLessonsLearned(conclusions) {
    let allLessonsLearned = Collection.retrieveAllLessonsLearned(conclusions)

    let serialized = allLessonsLearned.map((aLessonsLearned) => {
      return aLessonsLearned.serialize()
    })

    return serialized
  }
}
