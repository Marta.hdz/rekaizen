import Conclusion from '../../domain/Conclusion'

let conclusions = []

export default class Collection {
  static createGoodPractice(goodPractice) {
    conclusions.push(goodPractice)

    return goodPractice
  }

  static retrieveGoodPractice(id) {
    let retrieved = conclusions.find((conclusion)=>{
      return (conclusion.is(id) && conclusion.isAGoodPractice())
    })
    return retrieved || Conclusion.asNull()
  }


  static retrieveAllGoodPractices(references) {
    let goodPractices = []

    references.forEach((reference) =>{
      const retrieved = this.retrieveGoodPractice(reference)
      if (retrieved.isAGoodPractice()){
        goodPractices.push(retrieved)
      }
    })

    return goodPractices
  }

  static createLessonLearned(lessonLearned) {
    conclusions.push(lessonLearned)

    return lessonLearned
  }

  static retrieveLessonsLearned(id) {
    let retrieved = conclusions.find((conclusion)=>{
      return (conclusion.is(id) && conclusion.isALessonLearned() )
    })
    return retrieved || Conclusion.asNull()
  }

  static retrieveAllLessonsLearned(references) {
    let lessonLearned = []

    references.forEach((reference) =>{
      const retrieved = this.retrieveLessonsLearned(reference)
      if (retrieved.isALessonLearned()){
        lessonLearned.push(retrieved)
      }
    })

    return lessonLearned
  }

  static drop() {
    conclusions = []
  }
}
