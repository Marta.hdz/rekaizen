
let collection = []

export default class Collection {
  static store(issue) {
    collection.push(issue)

    return issue
  }

  static retrieve(issue) {
    let found = collection.find(element => element.isEqualTo(issue))

    return found
  }

  static retrieveAll() {
    return collection
  }
}
