import React from 'react'
import ReportModal from './ReportModal'
import ReportContent from './ReportContent'


class Report extends React.Component {
  render() {
    return (
      <React.Fragment>
        {this.props.show &&
          <div className="report-for-print">
            <ReportContent {...this.props} />
          </div>
        }
        <ReportModal {...this.props} />
      </React.Fragment>
    )
  }
}

export default Report
