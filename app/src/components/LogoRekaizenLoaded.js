import React from 'react'
import LogoRekaizen from './LogoRekaizen'

const LOADED_COLOR = "#77d353";
const FIVE_MINUTES_COLOR = "#d61216";

class LogoRekaizenLoaded extends React.Component {
  calculateXWarning() {
    const warningPercentX = this.props.timebox ? 500 / this.props.timebox : 0
    return `${warningPercentX}%`
  }
  render() {
    return (
      <LogoRekaizen timebox={this.props.timebox} color={LOADED_COLOR}>
        <clipPath id="five-minutes-warning">
          <rect
            x={this.calculateXWarning()}
            y="0"
            width="8.4"
            height="100%"
          />
        </clipPath>
        <use className='no-print'
          clipPath="url(#five-minutes-warning)"
          href="#rekazien-svg"
          fill={FIVE_MINUTES_COLOR}
        />
      </LogoRekaizen>
    )
  }
}

export default LogoRekaizenLoaded
