import Container from './Container'
import Renderers from '../renderers'

class Issue extends Container {
  constructor(bus) {
    super(bus)
    const callbacks = {
      createIssue: this.createIssue.bind(this)
    }
    this.renderer = Renderers.issue(callbacks)
  }

  createIssue(issue) {
    this.bus.publish('create.issue', issue)
  }

  subscriptions() {
    this.bus.subscribe('close.landing', this.showCreateIssue.bind(this))
    this.bus.subscribe('got.timeboxes', this.updateTimeboxes.bind(this))
    this.bus.subscribe('got.translations', this.updateTranslations.bind(this))
    this.bus.subscribe('new.issue', this.showCreateIssue.bind(this))
  }

  updateTimeboxes(payload) {
    this.renderer.update({
      timeboxOptions: payload.options,
      timeboxMoreOptions: payload.moreOptions
    })
  }

  showCreateIssue() {
    this.renderer.showCreateIssue()
  }

  render() {
    this.renderer.draw()
  }

}

export default Issue
