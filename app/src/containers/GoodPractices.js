import Renderers from '../renderers'
import Container from './Container'
import Orderer from '../libraries/Orderer'

class GoodPracticesContainer extends Container{
  constructor(bus){
    super(bus)
    const callbacks = {
      create: this.createGoodPractice.bind(this)
    }
    this.renderer = Renderers.goodPractices(callbacks)
  }

  subscriptions() {
    this.bus.subscribe('created.issue', this.retrieveIssue.bind(this))
    this.bus.subscribe('restore.issue', this.askGoodPractices.bind(this))
    this.bus.subscribe('got.translations', this.updateTranslations.bind(this))
    this.bus.subscribe('created.goodPractice', this.askCollection.bind(this))
    this.bus.subscribe('retrieved.goodPractices', this.updateGoodPractices.bind(this))
    this.bus.subscribe('new.issue', this.clean.bind(this))
  }

  askCollection() {
    this.bus.publish('retrieve.goodPractices', {issue: this.issue.id})
  }

  askGoodPractices(issue) {
    this.retrieveIssue(issue)
    this.bus.publish('retrieve.goodPractices', {issue: this.issue.id})
  }

  createGoodPractice(value) {
    let payload = { description: value, issue: this.issue.id }
    this.bus.publish('create.goodPractice', payload)
  }

  updateGoodPractices(payload) {
    let orderedPayload = Orderer.byBirthdayNewestToOldest(payload)
    this.renderer.update(orderedPayload)
  }

  clean() {
    this.retrieveIssue({})
    this.renderer.update([])
  }

  render() {
    this.renderer.draw()
  }
}

export default GoodPracticesContainer
