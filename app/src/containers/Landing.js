
import Container from './Container'
import Renderers from '../renderers'

class Landing extends Container {
  constructor(bus) {
    super(bus)
    this.renderer = Renderers.landing({
      closeLanding: this.closeLanding.bind(this)
    })
  }

  subscriptions () {
    this.bus.subscribe("got.translations", this.updateTranslations.bind(this))
    this.bus.subscribe("restore.issue", this.hide.bind(this))
  }

  closeLanding(){
    this.bus.publish('close.landing')
  }

  hide() {
    this.renderer.hide()
  }

  render () {
    this.renderer.draw()
  }
}

export default Landing
