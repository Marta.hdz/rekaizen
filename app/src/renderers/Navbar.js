import ReactDOM from 'react-dom'
import React from 'react'
import Renderer from './Renderer'
import NavbarComponent from '../components/Navbar'

class Navbar extends Renderer {
  constructor(callbacks) {
    super(callbacks)
    this.id = 'navbar'
    this.data = {
      showAlert: false,
      timebox: 0,
      description: '',
      text: '',
      translations: {
        rekaizen: '',
        finish: '',
        goodPracticeAdded: '',
        lessonLearnedAdded: '',
      }
    }

    this._currentAlertTimeoutRef = null
  }

  hideAlert() {
    clearTimeout(this._currentAlertTimeoutRef)
    this._currentAlertTimeoutRef = setTimeout(() => {
      this.displayData({ showAlert: false })
    }, 5000)
  }

  setIssueDescription(description) {
    this.displayData({ description: description })
  }

  setTimebox(timebox) {
    this.displayData({ timebox: timebox })
  }

  alert(text) {
    this.displayData({
      showAlert: true,
      text: text
    })
    this.hideAlert()
  }

  translate(payload) {
    const translations = {
      rekaizen: payload.rekaizen,
      finish: payload.finish,
      goodPracticeAdded: payload.goodPracticeAdded,
      lessonLearnedAdded: payload.lessonLearnedAdded
    }
    this.displayData({ translations: translations })
  }

  alertLessonLearnedAdded() {
    this.alert(this.data.translations.lessonLearnedAdded)
  }

  alertGoodPracticeAdded() {
    this.alert(this.data.translations.goodPracticeAdded)
  }

  draw(){
    ReactDOM.render(
      <NavbarComponent
        text={this.data.text}
        showNotification={this.data.showAlert}
        timebox={this.data.timebox}
        description={this.data.description}
        translations={this.data.translations}
        resumeIssue={this.callbacks.resumeIssue}
      />,
      document.querySelector('#' + this.id)
    )
  }

}

export default Navbar
