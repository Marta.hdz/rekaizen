import React from 'react'
import ReactDOM from 'react-dom'
import Renderer from './Renderer'
import ColumnAnalysis from '../components/ColumnAnalysis'

class Positives extends Renderer {
  constructor(callbacks) {
    super(callbacks)
    this.id = 'positives'
    this.data = {
      showTextArea: false,
      translations: {
        add: '',
        listTitle: ''
      },
      positives: []
    }
  }

  translate (payload) {
    const translations = {
      add: payload.addPositive,
      listTitle: payload.positives
    }
    this.displayData({ translations: translations })
  }

  update(collection) {
    this.displayData({ positives: collection })
  }

  updateOne(positive) {
    this.data.positives.push(positive)
    this.update(this.data.positives)
  }

  hideTextArea(){
    this.displayData({showTextArea: false})
  }

  showTextArea() {
    this.displayData({showTextArea: true})
  }

  draw() {
    ReactDOM.render(
      <ColumnAnalysis
        icon={<i className="fas fa-arrow-up icon" />}
        id={this.id}
        translations={this.data.translations}
        collection={this.data.positives}
        onClick={this.showTextArea.bind(this)}
        create={this.callbacks.create}
        remove={this.callbacks.remove}
        hide={this.hideTextArea.bind(this)}
        show={this.data.showTextArea}
        />,document.querySelector('#' + this.id)
    )
  }
}

export default Positives
