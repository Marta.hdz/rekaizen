import Bus from '../../../src/infrastructure/bus'
import Subscriber from '../../services/subscriber'
import Actions from '../../../src/api/actions'

describe('Retrieve TimeBoxes Action', () => {
  it('retrieves the timeBoxes options', () => {
    const bus = new Bus()
    const subscriber = new Subscriber('got.timeboxes', bus)

    Actions.retrieveTimeBoxes(bus)

    expect(subscriber.hasBeenCalledWith()).toStrictEqual(timeBoxOptions())
  })

  function timeBoxOptions() {
    return {
      options: [5, 10, 15, 20, 25, 30],
      moreOptions: [35, 40, 45, 50, 55, 60]
    }
  }
})
