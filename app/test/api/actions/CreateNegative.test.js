import Issues from '../../../src/api/services/issues'
import Subscriber from '../../services/subscriber'
import Issue from '../../../src/api/domain/Issue'
import Bus from '../../../src/infrastructure/bus'
import Actions from '../../../src/api/actions'

describe('CreateNegative Action', () => {
  let bus, issue, description

  beforeEach(() => {
    bus = new Bus()

    issue = new Issue('description', 45)
    Issues.collection.store(issue)
    description = 'A negative'
  })

  it('creates one', () => {
    const subscriber = new Subscriber('created.negative', bus)
    Actions.createNegative(bus)

    bus.publish('create.negative', { description: description, issue: issue.id })

    expect(subscriber.hasBeenCalledWith().description).toBe(description)
  })
})
