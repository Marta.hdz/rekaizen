import Issue from '../../src/components/Issue'
import {render, fireEvent, cleanup} from 'react-testing-library'
import React from 'react'

describe('Issue', () => {
  beforeEach(cleanup)

  it('the input has the focus', () => {
    const issue = render(<Issue id="issue" show={true} translations={{}} />).container
    const input = issue.querySelector('#description-issue-input')

    expect(document.activeElement).toEqual(input);
  })

  it('when click in button submit issue description', () => {
    const submit = jest.fn()
    const issue = render(<Issue
      id="issue"
      show={true}
      translations={{}}
      createIssue={submit}
      timeboxOptions={[10]}
    />).container

    const button = issue.querySelector('#description-issue-button')
    const input = issue.querySelector('#description-issue-input')
    fireEvent.change(input, { target: { value: 'description' } })

    fireEvent.click(button)

    const confirmTimeboxButton = issue.querySelector('#timebox-option-10')
    fireEvent.click(confirmTimeboxButton)

    const confirmIssueButton = issue.querySelector('#confirm-issue')
    fireEvent.click(confirmIssueButton)

    expect(submit).toBeCalled();
  })

  it("submit button is disabled if input has not text", () => {
    const submit = jest.fn()
    const issue = render(<Issue
      id="issue"
      show={true}
      translations={{}}
      createIssue={submit}
    />).container

    const button = issue.querySelector('#description-issue-button')

    expect(button.disabled).toBeTruthy()
  })
})
